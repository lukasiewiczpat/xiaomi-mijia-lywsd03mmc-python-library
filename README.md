# Xiaomi Mijia LYWSD03MMC python library

The library allows connecting to temperature and humidity sensor - Xiaomi Mijia LYWSD03MMC via Bluetooth. There are a few methods like a battery level read or a measurement subscription which give opportunity to use sensor in a simple and convenient way. The solution bases on [pygatt module](https://pypi.org/project/pygatt/ "pygatt module") and require USB adapter BLED112.

## Prerequisites

All required files are attached in a \lib catalogue, so there is no need to download additional modules and the library performs correctly with both Windows and Linux (tested with Raspbian). Notice that one line has to be changed for Linux usage, see LYWSD03MMC_lib.py.

```python
self._adapter=pygatt.BGAPIBackend(serial_port=self._dongle_COM_port)#comment for Raspberry Pi
#self._adapter=pygatt.BGAPIBackend()#uncomment for Raspberry Pi
```

To run the library specific hardware is required (shown in the picture below):
* Xiaomi Mijia LYWSD03MMC
* USB adapter BLED112

![hardware photo](https://gitlab.com/lukasiewiczpat/xiaomi-mijia-lywsd03mmc-python-library/-/raw/master/Photos/photo1.jpg)

## Quick start

If you want to run this software quickly, perform following procedure:

1. Set port COM and bluetooth address in ```example.py``` file.

```python
serial_port="COM12"
address="A4:C1:38:34:38:19"
```
2. Run ```example.py``` module.


## License

Xiaomi Mijia LYWSD03MMC python library is distributed under the MIT License, see the [LICENSE](https://gitlab.com/lukasiewiczpat/xiaomi-mijia-lywsd03mmc-python-library/-/blob/master/LICENSE.txt "LICENSE") file.

## Screenshots

![screenshot 1](https://gitlab.com/lukasiewiczpat/xiaomi-mijia-lywsd03mmc-python-library/-/raw/master/Screenshots/screenshot1.bmp)

![screenshot 2](https://gitlab.com/lukasiewiczpat/xiaomi-mijia-lywsd03mmc-python-library/-/raw/master/Screenshots/screenshot2.bmp)