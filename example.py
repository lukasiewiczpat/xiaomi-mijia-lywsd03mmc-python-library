'''
Example usage of LYWSD03MMC_lib.py
'''

import LYWSD03MMC_lib
import time

# Dongle and sensor parameters
serial_port="COM6"
address="A4:C1:38:34:38:19"

# Sensor object initialization
xiaomi_mijia=LYWSD03MMC_lib.LYWSD03MMC(dongle_COM_port=serial_port, sensor_address=address)

adapter_initialization_OK, device_connection_OK=xiaomi_mijia.connect_via_dongle()

if adapter_initialization_OK==True and device_connection_OK==True:
    # If connection is established

    # Read and print battery level
    xiaomi_mijia.battery_level_read()

    print("\n         ##########")
    print(f"    Battery level: {xiaomi_mijia._battery_level} [%]")
    print(f"    Battery level read time stamp: {xiaomi_mijia._battery_level_time_stamp_log}")
    print("         ##########")

    # Start reading and wait
    # There are threads which wait for notification and maintain communication, so main thread may be stopped by time.sleep function
    xiaomi_mijia.start_reading_temperature_and_humidity()
   
    time.sleep(300) # Specify how long app should read value from sensor 

    # Stop reading and disconnect
    xiaomi_mijia.stop_reading_temperature_and_humidity()
    xiaomi_mijia.disconnect()

    print("Disconnected")
    time.sleep(2)

else:
    print("Can't connect to sensor")