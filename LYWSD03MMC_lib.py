'''
The LYWSD03MMC_lib.py module allows connecting temperature and humidity sensor - Xiaomi Mijia LYWSD03MMC, using pygatt module and USB adapter BLED112.
'''

import os.path
import sys
import time
from datetime import datetime
import threading

# Import library modules
app_path=os.path.dirname(os.path.realpath(__file__))
lib_path= os.path.join(os.path.dirname(os.path.realpath(__file__)),"lib")

sys.path.append(lib_path)

import pygatt
from pygatt import util

degree_sign= u'\N{DEGREE SIGN}'



class LYWSD03MMC(object):


    def __init__(self, dongle_COM_port, sensor_address):
        '''
        Initializes Xiaomi Mijia LYWSD03MMC object.
        ---
        Parameters:
        _dongle_COM_port (string): port COM created by USB adapter (example: "COM4")
        _sensor_address (string): Xiaomi Mijia bluetooth address (example: "A4:C1:38:34:38:19")
        ---
        Attributes:
        _dongle_COM_port (string): port COM created by USB adapter (example: "COM4")
        _sensor_address (string): Xiaomi Mijia bluetooth address (example: "A4:C1:38:34:38:19")
        _adapter (object): pygatt library object which refers to USB dongle
        _device (object): pygatt library object which refers to sensor
        _control_loop_thread (thread object): thread starts when sensor connects. It controls connection and tries to reconnect if is lost.
        _connected (bool): connection flag
        _connected_control_loop_prev (bool): previous value of self._connected in connection control loop. Value is used to work out disconnection moment.
        _temperature (float): last temperature read [°C]
        _humidity (int): last humidity read [%]
        _temperature_humidity_time_stamp_log (string): contains information about time of last read of temperature and humidity
                                                       (format -> "YEAR/MONTH/DAY    HOUR:MINUTE:SECOND")
        _battery_level (float): last battery level read [%]
        _battery_level_time_stamp_log (string): contains information about time of last battery level read
                                                (format -> "YEAR/MONTH/DAY    HOUR:MINUTE:SECOND")
        '''

        # Attributes
        self._dongle_COM_port=dongle_COM_port
        self._sensor_address=sensor_address        

        self._adapter=None
        self._device=None

        self._control_loop_thread=None

        self._connected=False
        self._connected_control_loop_prev=False
        self._connection_control_loop_running=False       

        self._temperature=float(0.0)
        self._humidity=int(0)
        self._temperature_humidity_time_stamp_log=""

        self._battery_level=float(0.0)
        self._battery_level_time_stamp_log=""



    def connect_via_dongle(self):
        '''
        Connect to sensor using BLED112 USB dongle.
        --
        Returns:
        adapter_initialization_OK, device_connection_OK (bool, bool): connection flags -> if value is: True, True connection is established.
        '''

        adapter_initialization_OK=False
        device_connection_OK=False

        # Adapter initialization
        try:
            print(f"\nTrying to initialize dongle on {self._dongle_COM_port}")
            self._adapter=pygatt.BGAPIBackend(serial_port=self._dongle_COM_port)#comment for Raspberry Pi
            #self._adapter=pygatt.BGAPIBackend()#uncomment for Raspberry Pi
            self._adapter.start()
            adapter_initialization_OK=True
            print(f"Adapter initialization OK")

        except:
            adapter_initialization_OK=False
            print(f"Adapter initialization FAILED")

        # Device connection
        if adapter_initialization_OK:

            try:
                print(f"\nTrying to connect with Xiaomi Mijia LYWSD03MMC on {self._sensor_address}")
                self._device=self._adapter.connect(self._sensor_address)
                device_connection_OK=True
                self._connected=True
                print(f"Device connection OK")

            except:
                self._adapter.stop()
                device_connection_OK=False
                print(f"Device connection FAILED")
            
        return adapter_initialization_OK, device_connection_OK



    def disconnect(self):
        '''
        Disconnect from sensor. Firstly, stop reading temperature and humidity if needed.
        '''

        if self._connection_control_loop_running:
            #if temperature and humidity is reading 
            self.stop_reading_temperature_and_humidity()
        self._connected=False
        self._adapter.stop()



    def battery_level_read(self):
        '''
        If connection is established, the method reads battery level (self._battery_level) and save reading time stamp ( self._battery_level_time_stamp_log).
        '''

        if self._connected:

            try:
                battery_uuid=util.uuid16_to_uuid(0x2A19)
                self._battery_level=int(self._device.char_read(uuid=str(battery_uuid), timeout=10)[0])

                now=datetime.now()
                self._battery_level_time_stamp_log=now.strftime("%Y/%m/%d    %H:%M:%S")

            except:
                print("ERROR: Error occurred while reading battery level.")
    
        else:
            print("ERROR: Sensor isn't connected, can't read battery level.")



    def start_reading_temperature_and_humidity(self):
        '''
        The method subscribes to the sensor characteristic (temperature and humidity change) and each notification calls notificaiton_callback_method().
        It also starts a thread which controls connection via connection_control_loop method.
        '''

        if self._connected:

            self._device.subscribe(uuid="ebe0ccc1-7a0a-4b0c-8a1a-6ff2997da3a6", callback=self.notificaiton_callback_method)


            self._connection_control_loop_running=True
            self._control_loop_thread=threading.Thread(target=self.connection_control_loop,)
            self._control_loop_thread.start()
                
        else:
            print("ERROR: Sensor isn't connected, can't start reading temperature and humidity.")


    
    def stop_reading_temperature_and_humidity(self):
        '''
        The method unsubscribes temperature and humidity notification and stops loop which controls connection.
        '''

        self._connection_control_loop_running=False
        time.sleep(0.2)
        self._device.unsubscribe(uuid="ebe0ccc1-7a0a-4b0c-8a1a-6ff2997da3a6")


    
    def connection_control_loop(self):
        '''
        The method uses own thread and controls connection with sensor when there is subscription to temperature and humidity notifications.
        In a case of connection lost (e.g. sensor moves away), application tries to reconnect and resubscribe.
        '''

        while True:

            if self._connection_control_loop_running:

                try:
                    rssi_value=self._device.get_rssi()

                    #print(f"RSSI VALUE: {rssi_value}")

                    if rssi_value==0:
                        self._connected=False

                        if self._connected==False and self._connected_control_loop_prev==True:
                            #if connection has been lost -> once after occurance
                            print("*** Connection has been lost ***")
                            
                        print("Trying to reconnect...")
                        self._device=self._adapter.connect(self._sensor_address)
                        self._device.subscribe(uuid="ebe0ccc1-7a0a-4b0c-8a1a-6ff2997da3a6", callback=self.notificaiton_callback_method)

                except:
                    print("Reconnection failed")
          

            else:

                break

            self._connected_control_loop_prev=self._connected  

            time.sleep(1)

    

    def notificaiton_callback_method(self, handle, value):
        '''
        The method calls periodically when app is subscribed to temperature and humidity read.
        '''

        temperature_int=int.from_bytes(value[0:2], byteorder='little', signed=True)

        self._temperature=float(temperature_int)/100
        self._humidity=int(value[2])

        now=datetime.now()
        self._temperature_humidity_time_stamp_log=now.strftime("%Y/%m/%d    %H:%M:%S")

        print("--------------------------------------------------------")
        print(f"    Temperature: {self._temperature} [{degree_sign}]")    
        print(f"    Humidity: {self._humidity} [%]")
        print(f"    Time stamp: {self._temperature_humidity_time_stamp_log} ")
        print("--------------------------------------------------------")